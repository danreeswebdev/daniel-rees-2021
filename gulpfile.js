var gulp = require("gulp"),
    sass = require('gulp-sass')(require('sass'));
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"), 
    sourcemaps = require("gulp-sourcemaps"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gulpCopy = require('gulp-copy'),
    flatten = require('gulp-flatten'),
    browserSync = require("browser-sync").create();

const { series } = require('gulp');

var paths = {
    styles: {
        // Source files
        src: "static/src/assets/scss/**/*.scss",
        // Compiled files will end up here
        dest: "static/css"
    },
    js: {
        // Source files
        src: "static/src/assets/js/*.js",
        // Compiled files will end up here
        dest: "static/js"
    },
    fonts: {
        // Source files
        src: "static/src/assets/fonts/**/*",
        // Compiled files will end up here
        dest: "static/fonts"
    },
    images: {
        // Source files
        src: "static/src/assets/images/**/*",
        // Compiled files will end up here
        dest: "static/images"
    }
};

// Define tasks after requiring dependencies
// SASS/CSS
function style() {
    return gulp
        .src(paths.styles.src)
        // Initialize sourcemaps before compilation starts
        .pipe(sourcemaps.init())
        // Use sass with the files found, and log any errors
        .pipe(sass())
        .on("error", sass.logError)
        // // Use postcss with autoprefixer and compress the compiled file using cssnano
        // .pipe(postcss([autoprefixer(), cssnano()]))
        // Now add/write the sourcemaps
        .pipe(sourcemaps.write())
        // Concat
        .pipe(concat("custom.css"))
        // Destination
        .pipe(gulp.dest(paths.styles.dest))
        // Add browsersync stream pipe after compilation
        .pipe(browserSync.stream())
}

// JS
function js(){
    return gulp
        .src(paths.js.src)
        .pipe(concat('custom.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.js.dest))
}

// Copy fonts
function copyFonts(){
    return gulp
        .src(paths.fonts.src)
        .pipe(flatten())
        .pipe(gulp.dest(paths.fonts.dest))
}

// Copy images
function copyImages(){
    return gulp
        .src(paths.images.src)
        .pipe(flatten())
        .pipe(gulp.dest(paths.images.dest))
}

// Watch function
function watch() {
    browserSync.init({
        // You can tell browserSync to use this directory and serve it as a mini-server
        // server: {
        //     baseDir: "/Users/danielrees/Sites/.../public/"
        // }
        // If you are already serving your website locally using something like apache
        // You can use the proxy setting to proxy that instead
        proxy: "http://localhost:10003/"
    });
    // gulp.watch takes in the location of the files to watch for changes
    // and the name of the function we want to run on change
    gulp.watch(paths.styles.src, style)
    gulp.watch(paths.js.src, js)
    gulp.watch(paths.images.src, copyImages)
}

function build(cb) {
    cb();
}

exports.build = build;
exports.copyFonts = copyFonts;
exports.copyImages = copyImages;
exports.js = js;
exports.style = style;
exports.watch = watch;
exports.default = series(style, js, copyFonts, copyImages);