const fadeInUp = gsap.utils.toArray('.fade-in-up');

fadeInUp.forEach((fade, i) => {
  const anim = gsap.fromTo(fade, {autoAlpha: 0, y: 50}, {duration: 1, autoAlpha: 1, y: 0});
  ScrollTrigger.create({
    trigger: fade,
    animation: anim,
    toggleActions: 'play none none none',
    once: true,
  });
});