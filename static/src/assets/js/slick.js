// Slick
$('.carousel-hero').slick({
  dots: false,
  arrows: true,
  prevArrow:"<img class='slick-prev' src='/wp-content/themes/danielrees/static/images/chevron-left.svg'>",
  nextArrow:"<img class='slick-next' src='/wp-content/themes/danielrees/static/images/chevron-right.svg'>",
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  speed: 500,
  fade: true,
  cssEase: 'linear'
});

$('.skills-carousel').slick({
  dots: false,
  arrows: true,
  prevArrow:"<img class='slick-prev' src='/wp-content/themes/danielrees/static/images/chevron-left.svg'>",
  nextArrow:"<img class='slick-next' src='/wp-content/themes/danielrees/static/images/chevron-right.svg'>",
  infinite: true,
  speed: 500,
  cssEase: 'linear',
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});